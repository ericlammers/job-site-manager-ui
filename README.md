Project has been moved to the [Job Site Manager Group](https://gitlab.com/Job-Site-Manager).

Find it here: [https://gitlab.com/Job-Site-Manager/job-site-manager-ui](https://gitlab.com/Job-Site-Manager/job-site-manager-ui).